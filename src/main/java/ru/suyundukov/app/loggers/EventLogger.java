package ru.suyundukov.app.loggers;

import ru.suyundukov.app.beans.Event;

public interface EventLogger {
    void logEvent(Event event);
    String getName();
}
