package ru.suyundukov.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Service;
import ru.suyundukov.app.Spring.AppConfig;
import ru.suyundukov.app.Spring.DBConfig;
import ru.suyundukov.app.Spring.LoggerConfig;
import ru.suyundukov.app.beans.Client;
import ru.suyundukov.app.beans.Event;
import ru.suyundukov.app.beans.EventType;
import ru.suyundukov.app.loggers.EventLogger;

import javax.annotation.Resource;
import java.util.Map;


@Service
public class App {

    @Autowired
    private Client client;
    @Value("#{ T(ru.suyundukov.app.beans.Event).isDay(8,17) ? "
            + "cacheFileEventLogger : consoleEventLogger }")
    public EventLogger defaultLogger;
    @Resource(name = "loggerMap")
    private Map<EventType, EventLogger> loggers;
    @Value("#{'Hello user ' + "
            + "( systemProperties['os.name'].contains('Windows') ? "
            + "systemEnvironment['USERNAME'] : systemEnvironment['USER'] ) + "
            + "'. Default logger is ' + app.defaultLogger.name }")
    private String startupMessage;
    public App() {
    }
    App(Client client, EventLogger defaultLogger,
        Map<EventType, EventLogger> loggersMap) {
        this.client = client;
        this.defaultLogger = defaultLogger;
        this.loggers = loggersMap;
    }
    public Client getClient() {
        return client;
    }
    public String getStartupMessage() {
        return startupMessage;
    }

    public EventLogger getDefaultLogger() {
        return defaultLogger;
    }

    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(AppConfig.class, LoggerConfig.class, DBConfig.class);
        ctx.scan("ru.suyundukov.app");
        ctx.refresh();

        App app = (App) ctx.getBean("app");

        System.out.println(app.getStartupMessage());

        Client client = ctx.getBean(Client.class);
        System.out.println("Client says: " + app.getClient().getGreeting());

        app.logEvents(ctx);

        ctx.close();
    }

    public void logEvents(ApplicationContext ctx) {
        Event event = ctx.getBean(Event.class);
        logEvent(EventType.INFO, event, "Some event for 1");

        event = ctx.getBean(Event.class);
        logEvent(EventType.INFO, event, "One more event for 1");

        event = ctx.getBean(Event.class);
        logEvent(EventType.INFO, event, "And one more event for 1");

        event = ctx.getBean(Event.class);
        logEvent(EventType.ERROR, event, "Some event for 2");

        event = ctx.getBean(Event.class);
        logEvent(null, event, "Some event for 3");
    }

    public void logEvent(EventType eventType, Event event, String msg) {
        String message = msg.replaceAll(getClient().getId(), getClient().getFullName());
        event.setMsg(message);

        EventLogger logger = loggers.get(eventType);
        if (logger == null) {
            logger = getDefaultLogger();
        }

        logger.logEvent(event);
    }

}